provider "aws" {
  region = "eu-west-3"
}

terraform {
  backend "s3" {
    bucket = "itw-qonto-tf-state"
    key    = "web.tfstate"
    region = "eu-west-3"
  }
}
