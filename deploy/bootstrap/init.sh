#!/bin/bash

# Docker install
sudo apt-get update
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io

# App
sudo docker login registry.gitlab.com -u "${gitlab_registry_user}" -p "${gitlab_registry_token}"
sudo docker run -d -p 80:8080 "${gitlab_web_image}"