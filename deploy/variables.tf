variable "ssh_public_key" {
  description = "Use this to specify the public used as a key pair for the EC2 instance"
}

variable "whitelisted_ip" {
  description = "Whitelisted IPs for SSH"
}

variable "gitlab_registry_user" {
  description = "User used to log to the gitlab registry"
}

variable "gitlab_registry_token" {
  description = "Token used to log to the gitlab registry"
}

variable "gitlab_web_image" {
  description = "Full path of the image that we will run and serve on our server"
}
