output "server_ip" {
  value = aws_instance.web.public_ip
}

output "web_lb" {
  value = aws_lb.web.dns_name
}
