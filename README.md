# Qonto Exercice

Althought it is perfectly possible to deploy the app manually, it was optimised to be automatically deployed by Gitlab CI.

## Pre-requisite

You need to have an s3 bucket with read-write permission to store the state of terraform (see here: https://www.terraform.io/docs/backends/types/s3.html)
If you want to use your own bucket, change "itw-qonto-tf-state" to your own in `./deploy/main.tf`

## How it works

For convenience, I used a gitlab project. So even in the local setup I will rely on the gitlab docker registry.
You can always change the "defaults" for the deploy in deploy/terraform.tfvars.

Everything happens on the CI.
After setting up the necessary secrets in the CI:

- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY
- TF_VAR_gitlab_registry_token
- TF_VAR_gitlab_registry_user

You just need to push this repo to a new Gitlab project.
What the CI will do:

- in the `server/` directory: build + push the image
- in the `deploy/` directory: `plan` the deploy
- wait for manual confirmation to `apply` the terraform plan from above

If you need to run locally:

- First build the docker image: `cd server && docker build -t <registry_of_choice>:latest && docker push <registry_of_choice>:latest`
- (If not using a gitlab repository) Edit the `deploy/user_data.sh` to `docker login` inside your own docker registry (or delete it entirely because you use a public repo)
- Deploy the terraform stack: `cd deploy && terraform apply` (provide the requested secrets, you might also want to edit the defaults in `deploy/terraform.tfvars` to SSH inside the EC2 instance)
- See that the LB answers correctly on port 80

## Architecture

This is a pretty simple architecture:

- an ubuntu web server
- a load balancer speaking to the web server

The specificity here is that the server will be terminated and a new one created each time the docker image changes.
This might look bad from afar but it would allow us to scale really easily when using a Autoscaling group.

Some details of how we re-deploy:

- state saving on s3
- user_data that changes when the docker image changes (triggering the termination and replacement of the full EC2 instance, seed `deploy/bootstrap/init.sh`)

## Possible improvements

- Create a new VPC instead of using the default one
- Use terraform locking to avoid CI clashing
- Create a base AMI with docker installed so that init script just need to login + pull the image
